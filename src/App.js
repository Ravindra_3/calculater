
import React, { Component } from 'react';
import KeyPad from './components/keyPad.jsx'
import Output from './components/output';
import Header from './components/header';
import Showhistory from './components/history';
import './App.css';



class App extends Component {
  state = { 
    result:'',
    history:[],
    showHistory:false
   }
 
  
  buttonPressed=buttonName=>{
    if(buttonName==='='){
    this.calculate()
    }
    else{
      if(buttonName==='c'){
        this.reset()
      }else if(buttonName==='ce'){
        this.backspace()
      }else

    this.setState(
      {result:this.state.result + buttonName
      }
    )
    } 
      }

      reset=()=>{
        this.setState(
          {result:''}
        )
      }
      backspace=()=>{
        try{
        this.setState(
          {result:this.state.result.slice(0,-1)}
        )}catch(e){
          this.setState(
          {result:'Error'}
          )}
      }


      calculate=()=>{
      
        try{
        this.setState({
          // expresion:this.state.result,
          history:this.state.result,
          result: eval(this.state.result),
          // expresion:''
         
         
        })}catch(e){
          this.setState(
            {result:'error'}
          )
        }
     

      } 
      

    //   handleOnToggleHistory() {
     
    //     this.setState(prevState => ({ showHistory: !prevState.showHistory }));
    // }
    // show(){

    //  return (this.state.history)
    // }
  
  
    

  render() { 
    return (
     
      <div className='App'>
        <Header/>
        
        <div className='calc-body'>
          <Output result={this.state.result}/>
          {/* <Showhistory
                
      
             anyHistory={this.state.history.length > 0} 
            onToggleHistory={this.handleOnToggleHistory}
          
          /> */}
          {/* <div>{this.show=this.show}</div> */}
        
          <KeyPad buttonPressed={this.buttonPressed}/>
         </div>
         </div>
      
     );
  }
}
 
export default App;
