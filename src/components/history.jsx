
import React, { Component } from 'react';
import PropTypes from 'prop-types';
const Showhistory = (props) => (
    <div className="control-panel my-2 mx-1">
        <button className="btn btn-block text-secondary" disabled={!props.anyHistory} onClick={props.onToggleHistory}>
            <i className="fa fa-history fa-2x"></i>
        </button>
    </div>
);

// Showhistory.defaultProps = {
//     anyHistory: false,
//     onToggleHistory: () => alert('toggle history')
// };




// );
Showhistory.defaultProps = {
    anyHistory: false,
    onToggleHistory: () => alert('toggle history')
};

 Showhistory.propTypes = {
    anyHistory: PropTypes.bool,
    onToggleHistory: PropTypes.func
};
export default Showhistory;
