
import React from 'react';

const Header = () => (
    <nav className="header navbar navbar-dark bg-dark">
        <div className="container">
            <div className="row mx-auto">
                <i className="fa fa-calculator fa-3x text-white my-auto"></i>
                <div className="h3 ml-3 my-auto text-light">Calculater</div>
            </div>
         </div>
   </nav>
);



export default Header;